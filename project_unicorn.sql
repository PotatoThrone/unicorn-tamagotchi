-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 24. Feb 2016 um 19:52
-- Server-Version: 10.1.9-MariaDB
-- PHP-Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `project_unicorn`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `colors`
--

CREATE TABLE `colors` (
  `color` varchar(20) NOT NULL DEFAULT '',
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `colors`
--

INSERT INTO `colors` (`color`, `gid`) VALUES
('Braun', 0),
('Schwarz', 0),
('Silber', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `food`
--

CREATE TABLE `food` (
  `foodid` int(11) NOT NULL,
  `foodname` varchar(40) DEFAULT NULL,
  `foodtype` varchar(40) DEFAULT NULL,
  `health` int(11) DEFAULT NULL,
  `fun` int(11) DEFAULT NULL,
  `hunger` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `food`
--

INSERT INTO `food` (`foodid`, `foodname`, `foodtype`, `health`, `fun`, `hunger`, `price`, `gid`) VALUES
(1, 'Normal Potato', 'Potato', 0, 0, 25, 26, 0),
(2, 'Blue Potato', 'Potato', 0, 0, 30, 30, 0),
(3, 'Purple Potato', 'Potato', 10, 0, 35, 35, 0),
(4, 'Pink Potato', 'Potato', 0, 10, 50, 40, 0),
(5, 'Golden Potato', 'Potato', 10, 10, 75, 100, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `graphics`
--

CREATE TABLE `graphics` (
  `gid` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `path` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `graphics`
--

INSERT INTO `graphics` (`gid`, `name`, `path`) VALUES
(0, 'no_picture', NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `shelf`
--

CREATE TABLE `shelf` (
  `contains` int(11) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `shelf`
--

INSERT INTO `shelf` (`contains`, `owner`, `number`) VALUES
(1, 743, 0),
(2, 743, 0),
(3, 743, 0),
(4, 743, 0),
(5, 743, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tail`
--

CREATE TABLE `tail` (
  `tid` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `tail`
--

INSERT INTO `tail` (`tid`, `price`, `gid`) VALUES
(0, 0, 0),
(1, 30, 0),
(2, 70, 0),
(3, 140, 0),
(4, 300, 0),
(5, 500, 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `unicorn`
--

CREATE TABLE `unicorn` (
  `uniid` int(11) NOT NULL,
  `name` varchar(40) DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `color` varchar(20) NOT NULL,
  `health` double DEFAULT NULL,
  `hunger` double DEFAULT NULL,
  `fun` double DEFAULT NULL,
  `tail_lvl` int(11) DEFAULT NULL,
  `owner` int(11) DEFAULT NULL,
  `dirty` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `unicorn`
--

INSERT INTO `unicorn` (`uniid`, `name`, `birth`, `color`, `health`, `hunger`, `fun`, `tail_lvl`, `owner`, `dirty`) VALUES
(9, 'test', '2016-02-24', 'Silber', 97.965277777782, 100, 0, 0, 743, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `username` varchar(40) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `coins` int(11) DEFAULT NULL,
  `gold` int(11) DEFAULT NULL,
  `settingtype` int(11) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`uid`, `username`, `password`, `coins`, `gold`, `settingtype`, `last_login`) VALUES
(743, 'Test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 0, 93790, 1, '2016-02-24 18:50:41');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`color`),
  ADD KEY `gid` (`gid`);

--
-- Indizes für die Tabelle `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`foodid`),
  ADD KEY `gid` (`gid`);

--
-- Indizes für die Tabelle `graphics`
--
ALTER TABLE `graphics`
  ADD PRIMARY KEY (`gid`);

--
-- Indizes für die Tabelle `shelf`
--
ALTER TABLE `shelf`
  ADD KEY `contains` (`contains`),
  ADD KEY `owner` (`owner`);

--
-- Indizes für die Tabelle `tail`
--
ALTER TABLE `tail`
  ADD PRIMARY KEY (`tid`),
  ADD KEY `gid` (`gid`);

--
-- Indizes für die Tabelle `unicorn`
--
ALTER TABLE `unicorn`
  ADD PRIMARY KEY (`uniid`),
  ADD KEY `owner` (`owner`),
  ADD KEY `tail_lvl` (`tail_lvl`),
  ADD KEY `color` (`color`);

--
-- Indizes für die Tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `food`
--
ALTER TABLE `food`
  MODIFY `foodid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT für Tabelle `unicorn`
--
ALTER TABLE `unicorn`
  MODIFY `uniid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT für Tabelle `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=744;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `colors`
--
ALTER TABLE `colors`
  ADD CONSTRAINT `colors_ibfk_1` FOREIGN KEY (`gid`) REFERENCES `graphics` (`gid`);

--
-- Constraints der Tabelle `food`
--
ALTER TABLE `food`
  ADD CONSTRAINT `food_ibfk_1` FOREIGN KEY (`gid`) REFERENCES `graphics` (`gid`);

--
-- Constraints der Tabelle `shelf`
--
ALTER TABLE `shelf`
  ADD CONSTRAINT `shelf_ibfk_1` FOREIGN KEY (`contains`) REFERENCES `food` (`foodid`),
  ADD CONSTRAINT `shelf_ibfk_2` FOREIGN KEY (`owner`) REFERENCES `user` (`uid`);

--
-- Constraints der Tabelle `tail`
--
ALTER TABLE `tail`
  ADD CONSTRAINT `tail_ibfk_1` FOREIGN KEY (`gid`) REFERENCES `graphics` (`gid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
